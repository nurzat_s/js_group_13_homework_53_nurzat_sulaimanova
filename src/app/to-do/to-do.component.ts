import { Component } from '@angular/core';
import {Event} from "@angular/router";

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent {
  task = '';

  toDoList = [
    {task: 'Buy milk'},
    {task: 'Walk with dog'},
    {task: 'Do homework'}
  ]

  onAddTask() {
    this.toDoList.push({
      task: this.task
    })
  }

  onDeleteTask(i: number) {
    this.toDoList.splice(i, 1);
  }

  changeTask(index: number, newTask: string) {
    this.toDoList[index].task = newTask;
  }


}
