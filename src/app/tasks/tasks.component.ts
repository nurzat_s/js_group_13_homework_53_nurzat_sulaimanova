import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent {
  @Input() task = '';
  @Output() delete = new EventEmitter();
  @Output() taskChange = new EventEmitter<string>();

  input = false;

  onDeleteClick () {
    this.delete.emit();
  }

  taskInput(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.taskChange.emit(target.value);
  }

  inputFocusOn() {
    this.input = !this.input;
  }


  setInputClass() {
    let className = '';
    if(this.input) {
      className = 'focus';
    }
    return ['input', className]
  }



}
